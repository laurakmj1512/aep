//throbber
function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  frameRate (25);  //try to change this parameter
  
 }
 
 function draw() {
  //80 bestemmer hvor gennemsigtig den er <80 mindre gennemsigtig.
   background(140,173,143,30);  //check this syntax with alpha value
   drawElements();
 }
 
 function drawElements() {
  //det her 6 nummer bestmmer hvor mange cirkler der er
   let num = 130;
   push();
   //move things to the center
   translate(width/2, height/2);
   /* 360/num >> degree of each ellipse's movement;
   frameCount%num >> get the remainder that to know which one
   among 8 possible positions.*/
   //360 grader  er graderne i en cirkel, den her funktion sørger for at cirklerne roterer
   let cir = 360/num*(frameCount%num);
   let cir2 = 360/num*(frameCount%num);
   rotate(radians(cir));
   noStroke();
   fill(70,120, 249);
   //the x parameter is the ellipse's distance from the center
   ellipse(100, 0, 35, 35);
   ellipse(100,0,45,3);
   pop();
   stroke(70, 120, 249, 18);

push();
   noStroke()
   fill(255,0,0);
   ellipse(windowWidth/2,windowHeight/2,40,30);
   fill(0,255,0);
   ellipse(windowWidth/2,378,25,3);
   ellipse(windowWidth/2,377,2,15);
   fill(255,255,255);
   ellipse(710,385,5,5); 
   pop();
 }
 

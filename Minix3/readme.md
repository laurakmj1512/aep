
![](Skærmbillede_2023-03-05_kl._22.20.15.png)



Link the koden

https://gitlab.com/laurakmj1512/aep/-/blob/main/Minix3/minix3sketch.js


Link til programmet

https://laurakmj1512.gitlab.io/aep/Minix3/index.html


What do you want to explore and/or express?

I really want to explore what the thropper looks like, and make it alive. I think the thropper looks like a caterpillar crawling around, so i made it into a centapede or caterpiller crawling in a circle. i also wanted to explore how to make little legs on the caterpillar.
I also wondered: what is movitatvating this caterpillar, why is it going in circles? So i decided to movtivate the caterpillar with a tomato.
I wanted to make a twist of the classic throbber, the same shape and function, but more interesting. 
I also really wanted to understand how the loops and the code works, which i think i succeded. I could confidently change numbers in the code and understand the syntax orders.

What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?

Everything in my function setup is ofcourse endless. And everything in the draw function is a constant update.

frameRate (25);

Framerate decide how often the screen is updating in the draw function, and is therefor a usefull syntax in this case. This is because the frame rate decide how fast the caterpillar is crawling. 

I have tried to explane during the code, and used some of the existing code from the book. I have adjusted most of the numbers to make the code and imagery my own.
the code described down under makes circles in two different sizes apear x amount from the center in x amount of speed, x amount of times its repeated in the angles of a circles..


//det her 6 nummer bestmmer hvor mange cirkler der er
   let num = 130;
   push();
   //move things to the center
   translate(width/2, height/2);
   /* 360/num >> degree of each ellipse's movement;
   frameCount%num >> get the remainder that to know which one
   among 8 possible positions.*/
   //360 grader  er graderne i en cirkel, den her funktion sørger for at cirklerne roterer
   let cir = 360/num*(frameCount%num);
   let cir2 = 360/num*(frameCount%num);
   rotate(radians(cir));
   noStroke();
   fill(70,120, 249);
   //the x parameter is the ellipse's distance from the center
   ellipse(100, 0, 35, 35);
   ellipse(100,0,45,3);
   pop();
   stroke(70, 120, 249, 18);



Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?


I cant think of a throbber i remember except for the ones i see often. This might emplicate that i didnt notice that the website had to load or maybe i didnt think about it as a throbber. 
I think throbbers are emplimented to make us forgive the wait, or to conceal the fault that i causing us to wait. Maybe the throb is made to make us symphathise with the computer, humanize it. Make us think that the computer need to think or consider. I think by making a throbber for your website it makes you like the website better. Sometimes when the throbber looks very simple i think the website is not that good. And sometimes when the throbber looks likes gears i feel like the website is of worse quality.

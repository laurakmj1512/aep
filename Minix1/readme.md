**What have you produced?**

I have produced a little piece of art work. It is not supposed to ressemble anything in particular, but to be interpreted by the viewer. I see a chicken, some see a face, some see a bullseye, some see a flower, and some see an abstract piece of art. I have decided to do it this way, because it sort of represents the way i see coding. Very abstract, and my views are changing often. It also represents the awe i have had from the different learning curves in the classroom. I myself, find myself to be very talentless in this coding world. While some of my mutual classmates, was able to make advanced, beautiful, and usefull coding in their first try. Maybe this is why i see a tired chicken, where my classmates see a bullseye og a flower.
I have used few but effective refferences. I didnt want to copy a code from the internet, and i didnt want to break my neck trying to create something i couldnt understand anayway. I wanted to make the code completly myself, with my own interpretation of the references and material. I wanted the code to show my coding journey, and i didnt feel the code would be authentic to this, if i were to copy a code. By that not saying that i wont do it in future projects, but at this moment, a selfmade a throughout understood code, seemed to be the direction i want to go in.
My code is very simple, one of my instrocters even said that it could be hard to find notes to talk about in an exam situation. I think, because my code is so short and simple, i think it can start many interesting conversations. Like how the motive is so easily understood, but still misunderstood. How that translates into other codes and websites. How coding laguage is so challenging to me, and how the lack of transparency in coding (in relation to human-spoken languages) Maybe form certain platform, and how its used to trick/nudge/help people.
The refferences i used was the ellipse() refference and the fill() refference, beside this is used the background() and createCanvas(). The piece consists of to circles and four ellipses. All in different colors and opacities. Basicly i used the ellipse() to create the circles and ellipse shapes. I typed in an extra varrieble in the ellipse shape, to create the elongated shape. I used the fill() reffence to fill in the the different shapes with color. I tried to play with transparency, on the four red shapes. I did this by adding an extra varrieble in the code, this number then decided how saturated the shapes was in color. 


![](https://laurakmj1512.gitlab.io/aep/Minix1/Sk%C3%A6rmbillede_2023-02-11_kl._16.24.21.png)




**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

I would describe my first encounter with coding as complicated.. I am very hopefull, and i really wish to do good. But to be honest my first try was a very frustrating experience. I feel like i have reached my cryptonyte, or like the end of my logical brain. i am so clueless in this, i think it might be because i dont 100% understand how coding works. i dont know... i dont know... i think that describes my first experience with coding. It has difinitely been the word i have said the most in this proces. If im also very vulnarable and honest.. i dont know if im supposed to(there we have i dont know again) It has awoken a new insecurity in myself, and it has quite frankly made me think of my relationship with perfectionism. And i have come to the conclusion that i have to work on it. Everyday i came home from these classes, my brain was completely burnt out, and i was quite distressed. I had panic attacks and a hard time geeting over them. Because i hate loosing control, and in programming i really do. Beacuse i am so bad at tech stuff, and seeing through these systems. Even wrtiting was hard for me, because i dont understand Gitlap.
But not to worry, i am hopefull this will get better doing the semester, and if not; i guess its another important view on coding in a society perspective.(That undertsanding/not understanding code really isnt going to change quality of life, and maybe that understanding code isnt for everyone to understand)

**How is the coding process different from, or similar to, reading and writing text?**

From my perspective its not even comparable. Reading and writing text for me is being creative, documenting important events and feelings. Its a way to understand other people, and a way to understand history. When i visualize reading and writing its every emotion, and all the colors. Coding on the other hand is sad and neutral. Not beacuse coding is restrictive, but beacuse it sort of feel artificial, and maybe also superficial. it is hard, and takes long, its uniform and bland. To me writing is more free, and has a lot of purpose, or can be written to be unpuposeful (is this a word?) And it still has value. Where a faulted code has zero value.
That was my opinion, but the question seem to want an answer based on the material and subject, so this my geuss, as to what my teachers want to read; 
There is no different between coding and writing/reading.

**What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**

I would describe my first encounter with coding as complicated.. I am very hopefull, and i really wish to do good. I also find people who possess this skill, very cool. Aspeically women, i find it to be a powermove, to be a programming woman. Aspecially in a primarelly male dominated subject. Kinda scary to consider that at some point women were living in a male programmed (also in relation to older times, when men made all the rules) world. Therefor i find programming to be so cool, because its an accessible (sort of) way to be a part of the programming of the world, or to have a understanding of what the systems want from us.

link to my code:
https://laurakmj1512.gitlab.io/aep/Minix1/index.html



let mic;

function setup() {
  
  
  createCanvas (windowWidth,windowHeight);
  background(199, 138, 34);

  
//aktiverer mikrofonen
  mic = new p5.AudioIn();
  mic.start();
} 

function draw() {
  // put drawing code here

//de her varriabel bestemmer den konstante position af cirklerne
//det var sjovt at arbejde med varriabler på den her måde
var px = 880;
var py = 390;
var px2 = 550;
var pydisk = 320; 
var pxstart = 680;
var pxremix = 760;


  //den her syntax kombi sørger for at mikrofonen virker med formen
  //den bestemmer lyden niveauet den kan modtage
  
  let volume = mic.getLevel();
  let mappedVolume = map(volume,0,1,0,500);
  console.log(mappedVolume);
//no outline
  noStroke();
  //sørger for at firkanten er centreret
  rectMode(CENTER);
  fill(167,168,171);
  //boombox
  rect(windowWidth/2,windowHeight/2,600,280,100);

  fill(89, 89, 92);
  ellipse(px,py,150+ mappedVolume);

  fill(89, 89, 92);
  ellipse(px2,py,150+ mappedVolume);

//disk
  rect(windowWidth/2,pydisk, 125,10,100);

  //knapper
  fill(245,70,12);
  rect(pxstart,py,20,20,10);
  rect(pxremix,py,20,20,10);
  rect(pxstart, 450,20,20,10);
  rect(pxremix,450,20,20,10);

//håndtag
  fill(89, 89, 92);
  rect(windowWidth/2, 180,450,20,100);
  rect (px,220,7,70);
  rect(px2,220,7,70);
  

//text
  push()
  let sted = 650;

  text("please dont stop your music",sted, 600);
  



}


# Minix4

![](Skærmbillede_2023-03-13_kl._00.34.59.png)

## links

sourcecode:
https://gitlab.com/laurakmj1512/aep/-/blob/main/Minix4/minix4sketch.js

Programmet:

https://laurakmj1512.gitlab.io/aep/Minix4/index.html



## reflections

### Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.

My tittle is "please dont stop your music, this is a referrence to Rihannas clubhit "please dont stop the music" 
Many people have a relationship with music, many find music to be important in their life. My boombox doesnt play music, but "plays" with your voice, by using your microphone. My project is trying to communicate that your voice, opponion and reflections matters and should be heard. Speak up for what you love, speak up about the thing you find to be unfair, and speak up when you need help.

### Describe your program and what you have used and learnt.

My program is a bunch of shapes, bundled together to make a boombox. This boombox does not play music but moves to the sound of your voice, underneath it says "please dont stop your music" 
If you want to read my code with desribtions, there is a link in the top ^^ 
I have learned a lot from this project, Its the first time i have felt comfertable with varriables, and used them of my own will. I also really liked the microfone use on the computer, and playing around with that. Again i have tried to make it really simple, but well understood and wellmade. Next time i think i will challenge myself with some moving text or shapes maybe.
I also really tried to think of a clever way to document my own feeling about programming in a project. And really think about why i made this particular piece of work. The last couple of weeks i have just made what i was capable of, and no more than that. But this week i had the freedom of choosing.

### Articulate how your program and thinking address the theme of “capture all.”

My take on the task focused on the sound the microphone received, its a take on the theory that the phone and computers are listening. I think everyone has tried talking to their friends or family about something, and suddenly a commercial of that very something pops up. My boombox is kind of meant to be a vessel for the things you want or dont want. 
 Datacapture is able to capture, your dreams, your secrets and your desires, It will use this to give you recommendations, commercials, find relevant videos and content for you. What if you yelled something in your computer, that you litteraly can see that it "hears" and it might s
 come true. Its kind of my take of a digital manifistaion with a serious layer. All these tech giants often describe the collecting of data as good for the consumer as well. But i feel like this sometimes is a superficial illusion. When i was making this i kept thinking of if the women in iran yelling their prayers for freedom in the computer, and the computer could make it happen. Also i do believe that the data collecting only uses the data that is profitable. 

### What are the cultural implications of data capture?

I always say i dont mind my phone taking my data. Because i dont have anything to hide. But if data is the new gold, i do feel a bit cheated, beacuse someone is using my numbers to capitalize of of. Maybe its kinda weird to sell your own data, i feel like its kind of emotional prostitution. I would be up for it, but im not sure its ethical.

im also conflictied, beacuse i do see datacapture as a kind of surveilance. But an unethical kind. LIke for example, if target knows that a women is pregnant beacuse she buys shampoo, target probably also knows when someone is very ill, or depressed, or maybe even criminal. 
Should we then act on this information? why is it okay asume pregnancy, but not depression, crime, or illness? By acting on these, would it even work or be helpfull, or invasive. And where is the invasive line drawn? i dont know but im sure the answers will be revealed in the years to come. 




# Minix 1 chapter 7

![](Skærmbillede_2023-04-11_kl._22.11.06.png)


## Links

Link til programmet:

https://laurakmj1512.gitlab.io/aep/Minix7/index.html


Link til koden: 

https://gitlab.com/laurakmj1512/aep/-/blob/main/Minix7/minix7sketch.js


## Hvilken miniX har du valgt?

Minix1 Because that is the most stressful piece of nothing i have created.


## Hvilke ændringer har du lavet og hvorfor?

I changed almost everything, making the original code simpler and adding a few new features. My first minix was created to express my feelings about programming in a simple way that would allow me to track my progress and understanding. While I maintained the simplicity, I adapted it to my current level of skill. Additionally, I wanted to keep the original concept of reflecting my feelings towards programming. The initial concept was inspired by inkblot tests used by psychologists to analyze a patient's mental state or personality. The viewer would interpret what they saw in the splotch. This time, I kept the same concept but presented it in a more positive light. The intention was to convey that programming is still abstract and challenging, but with time and effort, one can learn and succeed. 

###### her bestemmer jeg hvor hurtigt cirklerne drejer rundt, samt canvas

function setup() {

  // put setup code here

createCanvas(windowWidth,windowHeight);

background(255,0,255);

frameRate(25);

}

###### Her bestemmer jeg baggrund, at der ikke er outline og de midterste cirkler: jeg sætter også en alpha værdi på min baggrund, så den fader ellipserne

function draw() {

  // put drawing code here

  background(255,0,255,30); 

  noStroke();

  fill(255,150,70);

ellipse(windowWidth/2, windowHeight/2,300);

fill(190,90,190);

ellipse(windowWidth/2, windowHeight/2,200);

  drawElements();

}

###### Her sørger jeg får at cirklerne drejer rundt i en cirkel. samt bestemmer cirklernes udsseende og farve.

  function drawElements() {
    
     let num = 130;
    
     push();
     translate(width/2, height/2);
    
     let cir = 360/num*(frameCount%num);
     let cir2 = 360/num*(frameCount%num);
     rotate(radians(cir));
     
     fill(200, 0, 30, 50);
     
     ellipse(210, 0, 160, 60);

     fill(200,0,30,50);

     ellipse(300,0,20,20);
    
     pop();


## Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?

I incorporated a bunch of syntaxes I learned throughout the semester. Additionally, I believe the subject of abstraction fits this project well. It's an abstraction of my views on programming and, in a way, an abstraction of programming itself.

I also thought about the subject of abstraction, because it is an abstraction of an inkblot. I generated it to be understood, so perhaps I have already established the rules that the viewer must follow and, therefore, their interpretation. I find this idea interesting. Can someone generate an opinion from my abstraction of feeling? Does that opinion add to my abstraction of feeling?

It's also interesting to consider this project in the context of the emoji assignment. Do 100 sizes compared to one size fit all, or do they make it more demanding and confusing? Am I imposing a mentality on someone by analyzing the inkblot, or am I expanding their creativity and expression? I suppose that depends on who you are, but how do you find your own feet in a pre-set rule set?

I also found it funny that in the context of data capture, this project is so useless. It doesn't even have an array of correct or incorrect answers, it simply just exists.


## Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?

How they are independant of eachother, with out the other there would no digital improvement, creativity, and etichal guideline. 
My project, i think shows that by combining these two, you will experience that the physical and digital roalm can melt together, and maybe they are closer than we think. I think its a interesting concept that something digital, made by numbers, can make us form an opponion and a felling. and by analysing the programming and the digital culture together, you will find out that you need both to make something of meaning. 





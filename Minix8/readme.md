Gruppe 10 - Elma , Laura & Lisa
Link til Figma fil online

Flowchart 1: Flower data
Vores første flowchart omhandler vores idé, vi har valgt at kalde Flower Data. Hele ideen bygger på, at man kan få en blomst til at spire ved at man indtaster data, derimod kan man få blomsten til at visne, hvis man ikke vælger at indtaste data.
Man begynder med at komme ind på vores hjemmeside/program, herinde ses der et billede af en blomst, og et tomt tekstfelt. I tekstfeltet kan der indtastes alle slags data. Hvis der i dette tekstfelt indtastes data indenfor 30 sekunder, bliver blomsten inde på hjemmesiden/programmet ved med at spire, og blomstens udseende ændres ved at billedet ændres. Man skal dog blive ved med at indsætte data indenfor 30 sekunder for at blomsten bliver ved med at spire.
Hvis der ikke bliver ved med at indsættes data, så begynder blomsten lige så stille at visne, stadie for stadie, som alt sammen kan ses på billedet af blomsten. Blomsten kan dog altid reddes, hvis man vælger at begynde at indtaste data, hvor den så stadie for stadie igen begynder at spire.

![](Skærmbillede_2023-05-01_kl._15.21.07.png)


Flowchart 2: Image Art
Vores anden flowchart omhandler ideen om en kunst generator, kaldet Image Art. Hele ideen bygger på at man tilgår vores hjemmeside/program hvor man ser en kunst generator. Når man kommer ind på siden, kan man vælge at indsætte et billede, så der kan udtrækkes RBG-data fra billedet. Hvis man ikke vælger at gøre det, kan der ikke udtrækkes RGB-data, og derfor kan kunstgeneratoren ikke genere kunst.
Vælger man derimod at indsætte et billede, kan RGB-dataet udtrækkes. Efter RGB-dataet bliver udtrukket, generer kunstgeneratoren kunstværker vha. af dette.

Read Me
What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
Det er altid svært at finde en måde, hvorved man kan kommunikere et budskab, så alle modtagere forstår det. I forbindelse med overvejelser vi har gjort os med kommunikationsniveauet og kompleksiteten af algoritmer kan vi se at der er en vigtig sammenhæng mellem de to ting.
Algoritmer findes på mange forskellige niveauer og jo mere komplekse algoritmen er opsat, jo mere kompliceret kan det udbytte/budskab der kommunikeres ud være. De overvejelser vi har gjort os i forhold til udfordringer, er at det er vigtigt at vi i algoritmen simplificerer selve algoritmens output. At vi forsøger at koge vores outputs sammen i stedet for at vi har flere forskellige og ligegyldige outputs der muligvis ikke er nødvendige.

![](Skærmbillede_2023-05-01_kl._15.21.38.png)


What are the technical challenges facing the two ideas and how are you going to address these?
Vi forudser at de største tekniske udfordringer vi kommer til at opleve, er i forbindelse med data capture. Dette er gældende ved begge af vores ideer.
Ved ideen Image Art tænker vi at vi muligvis får udfordringer ved at kode programmet, så det ikke kun indsamler et sæt RGB-data. Det vil sige at programmet ikke kun indhenter en farve. Vi ønsker at lave et stykke visuel kunst, der gør brug af flere forskellige farver. Derfor har vi brug for et stykke kode der indhenter flere farver. Derefter skal denne data som sagt også implementeres i den kode der genererer kunsten. Ved dette forestiller vi også visse udfordringer. Koden, der generer kunsten, skal også kunne modtage vores data capture og anvende det korrekt.
Flower data, som er vores anden ide, forudser vi visse problematikker i forhold til blomstens blomstrings niveauer. Som forklaret tidligere visner eller blomster blomstrer alt efter om der indtastes data. Hvordan vi skal opsætte disse niveauer, både i form af visning af blomsten og hvordan programmet reagerer på data inputtet/hvilken tilstand blomsten befinder sig i, har vi visse bekymringer om.
Derudover er en af de vigtigste conditions i programmet Flower Data selve reglen om tid - altså hvis der ikke indtastes data inden for 30 sekunder visner blomsten. hvordan vi skal opstille netop denne condition og sørge for at det bliver eksekveret korrekt er vi på nuværende tidspunkt stadig i tvivl om.



In which ways are the individual and the group flowcharts you produced useful?
De flowcharts vi har lavet i gruppen, håber vi senere hen kan bygge grundlag for vores afsluttende projekt, nemlig MiniX11. De to flowcharts formidler trin for trin hvordan vi ønsker at vores program skal fungere, og eksekveres, derfor er dette en hjælpende hånd når vi senere hen skal i gang med at kode vores idé.
Ligeledes, skal vi til vores afsluttende projekt lave en flowchart, der dykker mere ned i koden. Vi synes personligt, at det til tider kunne være svært at knække algoritmer, og koder ned i helt basale flowcharts, så det har helt klart været rart at kunne eksperimentere med det inden vores afsluttende projekt. Samt få noget erfaring i forhold til hvordan man bruger flowcharts i forbindelse med at visualisere koden/programmets proces.

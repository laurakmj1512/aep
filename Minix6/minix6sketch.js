let player;
let circles = []
let score = 0

function setup() {
  createCanvas(windowWidth, windowHeight);
  player = new Player();

  noStroke();
}

function draw() {
  background(150, 158, 181);

  // Få cirkel til at dukke op hver 60. frame
  if (frameCount % 60 == 0) {
    circles.push(new Circle());
  }

  // bevæg og tegn cirkler
  for (let i = 0; i < circles.length; i++) {
    circles[i].move();
    circles[i].draw();

    // check sammenstød med spiller
    if (circles[i].collidesWith(player)) {
      circles.splice(i, 1);
      score--;
    }

    // Fjerner cirkler i bunden af skærmen
    if (circles[i].y > height) {
      circles.splice(i, 1);
      score++;
    }
  }

  // Bevæger og tegner spilleren
  player.move();
  player.draw();

  // Viser point
  fill(0);
  textSize(32);
  text(score, 10, 30);

  // checker om spilleren har tabt+ generere tabt skift på skærmen
  if (score <= -3) {
    fill(255, 0, 0);
    textSize(64);
    text("YOU LOSE", windowWidth/2 - 150, windowHeight/2);
    noLoop();
}

}
//Bestmmer hvad spilleren kan
class Player {
  constructor() {
    this.posX = windowWidth / 2;
    this.posY = windowHeight - 50;
    this.speed = 5;
    this.width = 50;
    this.height = 50;
  }

  move() {
    if (keyIsDown(LEFT_ARROW)) {
      this.posX -= this.speed;
    }
    if (keyIsDown(RIGHT_ARROW)) {
      this.posX += this.speed;
    }
    // constrain player to the screen
    this.posX = constrain(this.posX, 0, windowWidth - this.width);
  }
//spilleren
  draw() {
    fill(235, 205, 188);
    rect(this.posX, this.posY, this.width, this.height);

        // Spillerens ansigtstræk
        //små øjne
        fill(250, 252, 252);
        ellipse(this.posX + this.width / 4, this.posY + this.height / 4, 10);
        ellipse(this.posX + 3 * this.width / 4, this.posY + this.height / 4, 10);
        //pupiller
        fill(0, 0, 0);
        ellipse(this.posX + this.width / 4, this.posY + this.height / 4, 3);
        ellipse(this.posX + 3 * this.width / 4, this.posY + this.height / 4, 3);

        fill(255,0,0);
        ellipse(this.posX + this.width / 2, this.posY + this.height + 1, 20);


      
  }
}

//Bestemmer hvordan ægget ser ud, hvor hurtigt og hvordan det bevæger sig.
class Circle {
  constructor() {
    this.posX = random(windowWidth);
    this.posY = 0;
    this.speed = 3;
    this.radiusX = 20;
    this.radiusY = 30; 
  }

  move() {
    this.posY += this.speed;
  }

  draw() {
    fill(253, 241, 184); // æggefarve
    ellipse(this.posX, this.posY, this.radiusX * 2, this.radiusY * 2);
  }

  collidesWith(other) {
    let dx = this.posX - max(other.posX, min(this.posX, other.posX + other.width));
    let dy = this.posY - max(other.posY, min(this.posY, other.posY + other.height));
    //Gør at man kan gøre det igen og igen.
    return (dx * dx + dy * dy) < (this.radiusX * this.radiusY);
  }
}


 





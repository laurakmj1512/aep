# Minix6-Ægge ægge pip pip
i Wrote this 3 times because Gitlap crashed-feel bad for me

Billede
![](Skærmbillede_2023-04-10_kl._14.55.50.png)


## Links

Programmet:

https://laurakmj1512.gitlab.io/aep/Minix6/index.html

Koden:

https://gitlab.com/laurakmj1512/aep/-/blob/main/Minix6/minix6sketch.js



## Describe how does/do your game/game objects work?

The point og the game is to not get hit by eggs, You have to control the little character safely through the egg rain.

let player;

let circles = []

let score = 0

function setup() {

  createCanvas(windowWidth, windowHeight);

  player = new Player();

  noStroke();
}

function draw() {

  background(150, 158, 181);

  // Få cirkel til at dukke op hver 60. frame


  if (frameCount % 60 == 0) {

    circles.push(new Circle());
  }

  ###### bevæg og tegn cirkler

  for (let i = 0; i < circles.length; i++) {

    circles[i].move();

    circles[i].draw();

###### Tjek sammenstød med spiller

    if (circles[i].collidesWith(player)) {

      circles.splice(i, 1);

      score--;
    }

###### Fjerner cirkler i bunden af skærmen

    if (circles[i].y > height) {

      circles.splice(i, 1);

      score++;
    }
  }

###### Bevæger og tegner spilleren

  player.move();

  player.draw();

###### Viser point

  fill(0);

  textSize(32);

  text(score, 10, 30);

###### tjekker om spilleren har tabt+ generere tabt skift på skærmen

  if (score <= -3) {

    fill(255, 0, 0);

    textSize(64);

    text("YOU LOSE", windowWidth/2 - 150, windowHeight/2);

    noLoop();
}

}
###### Bestmmer hvad spilleren kan

class Player {

  constructor() {

    this.posX = windowWidth / 2;

    this.posY = windowHeight - 50;

    this.speed = 5;

    this.width = 50;

    this.height = 50;
  }

  move() {

    if (keyIsDown(LEFT_ARROW)) {

      this.posX -= this.speed;

    }

    if (keyIsDown(RIGHT_ARROW)) {

      this.posX += this.speed;

    }

    // constrain player to the screen

    this.posX = constrain(this.posX, 0, windowWidth - this.width);

  }
###### spilleren

  draw() {

    fill(235, 205, 188);

    rect(this.posX, this.posY, this.width, this.height);

###### Spillerens ansigtstræk

###### små øjne

        fill(250, 252, 252);

        ellipse(this.posX + this.width / 4, this.posY + this.height / 4, 10);

        ellipse(this.posX + 3 * this.width / 4, this.posY + this.height / 4, 10);

###### pupiller

        fill(0, 0, 0);

        ellipse(this.posX + this.width / 4, this.posY + this.height / 4, 3);

        ellipse(this.posX + 3 * this.width / 4, this.posY + this.height / 4, 3);


        fill(255,0,0);

        ellipse(this.posX + this.width / 2, this.posY + this.height + 1, 20);


      
  }
}
###### Bestemmer hvordan ægget ser ud, hvor hurtigt og hvordan det bevæger sig.

class Circle {

  constructor() {

    this.posX = random(windowWidth);

    this.posY = 0;

    this.speed = 3;

    this.radiusX = 20;

    this.radiusY = 30; 

  }

  move() {

    this.posY += this.speed;

  }

  draw() {

    fill(253, 241, 184); // æggefarve

    ellipse(this.posX, this.posY, this.radiusX * 2, this.radiusY * 2);

  }

  collidesWith(other) {

    let dx = this.posX - max(other.posX, min(this.posX, other.posX + other.width));

    let dy = this.posY - max(other.posY, min(this.posY, other.posY + other.height));

    //Gør at man kan gøre det igen og igen.


    return (dx * dx + dy * dy) < (this.radiusX * this.radiusY);
  }
}





## Describe how you program the objects and their related attributes, and the methods in your game.

To desbribe it a way i understand it myself: You create different shapes. You descide the look, mood, size and color of this character. Then you make rules for the characters. In this instance the rules for my character shape is to move when left/right arrow is pressed, and to tell the point counter when its hit by an egg. The Point counter shapes rules are to change to the next number when its told by the character. The egg shapes rules is to go from the top of the screen to the bottom of the screen in a chosen speed. 

In my head it makes sense that i just translated the text above into programming. 



## Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?,
## Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?

I find abstractions interesting when it comes to language. I read somewhere that programming is an abstration of human spoken language. I thought about this, and i struggle to see two languages to  be abstractions of one another. Human text and letters are translated directly into sound. Where programming syntaxes are translated into actions. The human language can use sound to desribe actions, but never execute an action by language. Can you say that when humam can use programming language, that it becomes human language? Well just because a hearing person uses sign language doesnt mean it can understand "deaf language". The signs are still connected to sound, but for a deaf person the sign is connected to a thought, feeling or energy. So my own little conclusion is that language isnt an abstraction of other languages, but of the available way that Person/ thing communicates. 


I have always thought of abstractions as something of material things or maybe a feeling, fx an abstract painting of a house. or an installation thats really dark and sad- an abstraction of fear or depression. I therefor wanted to make my game with the intend of making a abstractation of etical human questions. kind of inspired by the quote underneath from the aestetic programming book.

"In his way, computational objects allow for a different perspective on lived conditions in this way and how we perceive the world. Worldviews can often be unethical, and we only need to think of game-worlds to see poor examples of racial and gendered abstraction that expose some of the assumptions of the world, and what properties and methods that these characters are being defined. Therein lies part of the motivation for this chapter, to understand that objects are designed with certain assumptions, biases and worldviews, and to make better object abstractions and ones with a clearer sense of purpose."


Abstractions of high morality, its interesting to think about the morality clock of people. In movies, games, and series, we always know whos is evil and who is the good one. And even if the main character is "mean" there is always an underlying reason or even higher moral. I thought it was funny to make an abstraction of this. If you get hit by eggs you loose, but if you dont, you have to play forever. This means you eventually hit the character on purpose, and ruin the high moral. Be mean or commit for life.

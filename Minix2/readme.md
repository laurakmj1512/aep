![](Skærmbillede_2023-02-19_kl._20.41.17.png)

**Describe your program and what you have used and learnt.**

I have made a DRAW ME/DRAW YOURSELF, its basicly a classic yellow circle that you can fill in yourself. 
This week i already feel progress in terms of curriousity and skill. In the first week i felt lost and sad, this week i feel more challenged and hopefull. I have learnt some different ways to make a code interactive, which i really enjoy playing with.

In the (function setup) (you can see this below the text) I have made a canvas, both the width and height of the window. On this canvas i have made a circle, this circle is in the setup function, because it needs to be a constant.
The circle is places in the middle by using /ellipse(windowWidth/2, windowHeight/2, 500,500);/ this works because you devide the window width and height by 2. 
The colors are decided by the /fill/ and /background/ option, i used the color tool, to find my RGB values. I wanted yellow hues, which is made by the RGB values below.

function setup() {

  // put setup code here

  //Den her kode bestemmer canvas str. og cirkel str./farve

  createCanvas(windowWidth,windowHeight);

  background(252,252,169);

  fill(252,252,23);

  ellipse(windowWidth/2, windowHeight/2, 500,500);


The (function draw) (you can see this below this text, as well as my variebles)
The goal was to be able to draw, when you click the mouse. I did this by making an /if/ translated it says "if mouse is pressed, black circles will be places where the mouse is clicked in size 5" i made it by pure luck-so im really proud of that one:)
To make the text i made two variables "s" and "t" these decide what the text is saying. Then just put it in the code, with placement and size.


let t = 'DRAW ME';

let s ='DRAW YOURSELF'


function draw() {

  // put drawing code here

  //Funktion danner klik og tegn effekt

  pop()

if(mouseIsPressed)

{fill(0,0,0);

  ellipse(mouseX,mouseY,5)};

push()

pop()

fill(50);

text(t,685,50,120,520);

push()

fill(50);

text(s,675,620,120,520);

I learnt a bunch of different functions this week, but i decided to keep it simple and understandable. I want to learn, to use the code for real a few functions at a time. I also want these minix assignments to represent my real progress.

**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?**

My emoji is called DRAW ME/DRAW YOURSELF, The design itself is simple and understandable. It is sort of simular to the idea behind "bitmoji".
The difference is that the base layer is a yellow circle. You therefor dont have the possiblility to make it resemble ypurself. The click and draw tool is constant, and cant be changed to another color/ thickness. This strips of the possibility to make something 'very' beautiful.
the "DRAW ME/DRAW YOURSELF" forced you to strip off the expectations you have to your looks and expression, and calls for raw emotion or reaction.
It also makes you kind of vulnerable, because you cant hide behind the universalism of the exiciting emojies. Fx if i want to express sadness in my emoji i have to look at myself/think to myself " how am i sad?, how do i look when i am sad?, how do i communicate my sadness?" This can start a thought process of evaluating induvidual emotion and the understanding of same.
The name of the piece means that the circle takes the burden of feeling YOUR feelings. This circle is not meant to represent you, its meant to represent what you feel. It means that "me and you" is the same right in this moment.  
This made me think and consider the question;
Do you really need your own face to represent your own feeling? 
After some thought my answer is no, i can express my feeling in writing, and some people in music, some art.
So maybe you dont need an emoji that replicates you exactly? maybe you just need a yellow circle.

I chose the color yellow because its a notch to the original emoji. i didnt choose it to provoke or advocate for something, but to incorperate some "affordance" or user experience knowlegde with having to describe how to use it. 

link til min kode

https://laurakmj1512.gitlab.io/aep/Minix2/index.html

Link til sourcecode

https://gitlab.com/laurakmj1512/aep/-/blob/main/Minix2/minix2sketch.js


# Minix5
![](Skærmbillede_2023-03-19_kl._20.54.01.png)

![](Skærmbillede_2023-03-19_kl._20.53.53.png)

## Links

Link til program:

https://laurakmj1512.gitlab.io/aep/Minix5/index.html

Link til sourcecode:

https://gitlab.com/laurakmj1512/aep/-/blob/main/Minix5/Minix5sketch.js


## What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?

I wanted my program to look like/ be inspired by a knitting diagram that you use in intarsia knitting. In knitting there is no randomness, everything is very specific and if dont you follow the order, you have no result. So my rules were **within a grid** create **subtle randomness**.
My program will within a tight grid, and time schedule, create subtle pattern changes, this is in a forever-loop and will never settle. 
By setting the forloop and random shapes within a defined grid, the different "knitting" patterns will emerge in small scale, and change in framerate(0,5).

![](Skærmbillede_2023-03-19_kl._20.53.38.png)

to start my canvas i used the usual:

**function setup() {

  createCanvas(windowWidth,windowHeight);

  fill(0)
  
  strokeWeight (3);

  frameRate(0.5);**

  I used the framerate() syntax to slow down the pattern change. An the strokeWeight() to decide the thickness of the lines.


I put the background()syntax in my function draw () to make sure the pattern completly changed every frame.

I used rectMode(CENTER) so that my squares in my for loop would be placed on the x/y axis by their center point and not by the corner.

After this i setup my varriables:

  let rum = 50

  let r= random(20);

  let er= random(50);

I decided on to different random varriables to make the pattern less predictable. 
rum is set to 50, to define the "grid spaces" to be 50x50 on the x and y axis.


After this i made the for loop.

First i define the grid, i make an invisible definition as to where the grid is. By grid i mean the squares that the pattern is build on, and why it repeats itself on the screen.
This is done by defining the size and the grid spaces and then placing them on the y/x axis. I guess you can i make the X/Y axis repeat itself on the canvas more than one time. Then i wrap this invisible in lines, these lines are in strokeWeight(3)as i defined earlier.

Then i place square(), these squares i place in the corner of every gridspace, and in the center of each gridspace. 

i repeat this step with the ellipse() 

i fill all the shapes with shades of brown, to prevent uncomfortable brightness, or epilepsy.


**for (x=0; x<width +50; x+= rum) {

    for (y=0; y<height +50; y+= rum){

      fill(128, 123, 111);

line(x,y,x+rum,y);

line(x,y,x,y+rum);


fill(128, 123, 111);

square(x,y,r);

  square(x+rum/2,y+rum/2,r);


  fill(43, 32, 5);

ellipse (x,y,er);

ellipse(x+rum/2,y+rum/2,er);**





## What role do rules and processes have in your work?

It has quite a big role, if i were to knit this pattern,it would be never endning. It would have no system or structure. But it would still be a knit. In knit there is syntaxes like in programming, randomizing the pattern/syntax like this would in theory not work, but yet this code shows that there are som sort of result anyway. 
because its an endless loop, it doesnt contribute anything in both the programming world and the knitting world. they both dont make any sense or add value to peoples life, because there are no point to them. I find the parallels between knitting and programming interesting, because they are so simular, and yet they exist in two different worlds, in many ways.

## Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?

I loved the litterature refering to auto-generating as an example about finding the strcuture in chaos. Fx the Love letter genrator, taking random words from a database and putting them together to form readable loveletters. Or about the caos in termite nest, from the outside looking like a mess, or chaotic. But the nest actually has a hierarchy and a structured system that they live by. And how that relates to programming. I found this quote from the aestetic programming particular interesting.

“A society defined entirely in terms of a functional model would correspond to the Aristotelian idea of natural hierarchy and order. Each official would perform the duties for which he [sic] has been appointed. These duties would translate at each level the different aspects of the organization of the society as a whole. The king gives orders to the architect, the architect to the contractor, the contractor to the worker. On the contrary, termites and other social insects seem to approach the ‘statistical’ model. As we have seen, there seems to be no mastermind behind the construction of the termites’ nest, when interactions among individuals produce certain types of collective behavior in some circumstances, but none of these interactions refer to any global task, being all purely local.” (page 137.)

it made me think of other structured establishment as generated. Also how real loveletters and love in general is taken from human made laguage templates. How structure in randomness, create programmable chaos.
I once read somewhere that there only is 13 different movie structures or character assortment. Maybe thats the same for more scenarios- That we only have a certain amount of structures or templates we use as humans in general, and that we are more programmed than we thought.










